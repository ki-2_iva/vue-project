import { createApp } from 'vue';
import { createStore } from 'vuex';

import App from './App.vue'

const store = createStore({
	state() {
		return {
			leftSideShapes: [],
			rightSideShapes: [],
		};
	},
	mutations: {
		addLeftSideShape(state, shape) {
			state.leftSideShapes.push(shape);
			console.log(state.leftSideShapes);
		}
	}
});

const app = createApp(App);

app.use(store);

app.mount('#app');